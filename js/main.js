
//--------------------------------------SLIDER----------------------------------
$(window).load(function () {
    $('.flexslider').flexslider();
});

//---------------------------------------POP UP----------------------------------
var elementos = document.querySelectorAll(".p-articulo");

//For que recorre los elementos que contienen la clase p-articulo
for (var i = 0; i < elementos.length; i++) {
    elementos[i].addEventListener("click", obtenerDatos);
}

//Funcion que obtiene los datos correspientes de la clase que se clickeo
function obtenerDatos() {
    //Variable que almacena el tag ingresado por parametro
    var body = document.getElementsByTagName('body')[0];
    //Variable  que almacena la clase correspondiente al POP-UP
    var contenedor = document.getElementsByClassName('informacion-pop');

    document.getElementById('close-data').innerHTML = this.dataset.close;
    document.getElementById('img-data').src = this.dataset.img;
    document.getElementById('precio-data').innerHTML = "Precio: " + this.dataset.precio;
    document.getElementById('nombre-data').innerHTML = this.dataset.nombre;
    document.getElementById('caracteristica-data').innerHTML = this.dataset.caracteristica;
    document.getElementById('color-data').innerHTML = "Color: " + this.dataset.color;
    document.getElementById('talle-data').innerHTML = "Talle: " + this.dataset.talle;
    document.getElementById('cuota-data').innerHTML = this.dataset.cuotas;
    document.getElementById('visa-data').src = this.dataset.tvisa;
    document.getElementById('maestro-data').src = this.dataset.tmaestro;
    document.getElementById('american-data').src = this.dataset.tamerican;
    document.getElementById('btn-data').innerHTML = this.dataset.btn;

    //Aplico estilos dinámicos
    var cerrarVentana = document.getElementById('close-data');
    cerrarVentana.onclick = function () { location.reload() };
    contenedor[0].style.backgroundColor = "white";
    contenedor[0].style.padding = "20px";
    contenedor[0].style.border = "5px solid black";
    imgProducto = document.getElementById('img-data');
    imgProducto.width = 350;
    imgProducto.height = 350;
    btnProducto = document.getElementById('btn-data');
    btnProducto.style.padding = "8px";
    btnProducto.onclick = function () { location.reload() };
    body.style.overflow = "hidden";
}

function desactivar(e) {
    alert("No podemos procesar tu solicitud intentelo de nuevo más tarde");
    location.reload();
}

//-----------------------MAPA---------------------------------------------------------
function iniciarMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -53.7874797, lng: -67.6989907 },
        zoom: 22,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marker = new google.maps.Marker({
        position: { lat: -53.7874797, lng: -67.6989907 }, map: map
    });
}

//--------------------------FORMULARIO-------------------------------------------------
var formulario = document.querySelector(".btn-enviar");

formulario.addEventListener("click", obtenerValidacion);

//Funcion que valida los datos ingresados en el formulario
function obtenerValidacion() {
    var nombre = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var asunto = document.getElementById('asunto').value;
    var mensaje = document.getElementById('mensaje').value;

    //Valida que todos los campos contengan datos
    if (nombre.value == 0 || email == 0 || asunto == 0 || mensaje == 0) {
        alert("Todos los campos son obligatorios");
    }

    //Valida el campo nombre
    if (nombre.length > 0 && nombre.length < 4) {
        alert("El campo nombre no debe contener menos de 4 caracteres");
    } else if (nombre.length > 30) {
        alert("El campo nombre no debe contener mas de 30 caracteres");
    } else {
        var patronNombre = /^[a-zA-Z\s]*$/;
        if (nombre.search(patronNombre)) {
            alert("El campo nombre no debe contener valores numericos");
        }
    }

    //Valida la direccion de correo
    if (email != 0) {
        var patronEmail = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
        var valido = patronEmail.test(email);
        if (valido == false) {
            alert("No se ha ingresado una direccion de correo valida");
        }
    }

    //Valida el campo asunto
    if (asunto.length > 20) {
        alert("El campo asunto no debe contener más de 20 caracteres");
    }

    //Valida el mensaje ingresado por el usuario
    if (mensaje.length > 0 && mensaje.length < 10) {
        alert("El campo mensaje no debe contener menos de 10 caracteres");
    } else if (mensaje.length > 100) {
        alert("El campo mensaje no debe contener mas de 100 carecteres");
    }

}
